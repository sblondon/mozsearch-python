# MozSearch Python

Add a search engine to search in last Python3 documentations (including standard library reference, language reference, C API, etc.) hosted by python.org.

A shortcut is available if you use awesome bar: @pyt

Available for users at:
https://addons.mozilla.org/fr/firefox/addon/python-search/

Released under GPL licence v3 or upper


About search engine in manifest file:
https://developer.mozilla.org/fr/docs/Mozilla/Add-ons/WebExtensions/manifest.json/chrome_settings_overrides
